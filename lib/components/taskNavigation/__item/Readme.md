Неактивные элементы:

    <div style={{backgroundColor:"#3C3442"}}>
    {
      ['lock','pure','good','perfect','error'].map((status,i) =>
        <TaskNavigationItem key={i} step={3} status={status} isActive={false} onClick={()=> null}/>
        )      
    }
    </div>

Активные элементы:

    <div style={{backgroundColor:"#3C3442"}}>
    {
      ['pure','good','perfect','error'].map((status,i) =>
        <TaskNavigationItem key={i} step={3} status={status} isActive={true} onClick={()=> null}/>
        )      
    }
    </div>
