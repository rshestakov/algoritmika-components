import { Component, PropTypes } from 'react';
import s from './TaskNavigationItem.css';


/**
 * Элемент навигации по уроку
 */
const TaskNavigationItem = (props) => {
  const {
    step,
    status,
    isActive,
    onClick
  } = props;
  return (
    <div className={s.root}>
      <div className={[s.wrap,isActive? s.active:''].join(" ") } onClick={onClick}>
        <div className={[s.icon, s[status]].join(" ")}>{step}</div>
      </div>
    </div>
  )
}

TaskNavigationItem.propTypes = {
  /**
   *  шаг задания
   */
  step: PropTypes.number.isRequired,
  /**
   * Статус выполнения.
   */
  status: PropTypes.oneOf(['lock','pure','good','perfect','error']).isRequired,
  /**
   * Статус активности шага
   */
  isActive: PropTypes.bool.isRequired,
  /**
   * on click callback
   */
   onClick: PropTypes.func.isRequired
}


export default TaskNavigationItem;
