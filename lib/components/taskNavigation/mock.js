export const items = [{
    step:1,
    status: "perfect",
    isActive: false
  },
  {
    step:2,
    status: "good",
    isActive: false
  },
  {
    step:3,
    status: "perfect",
    isActive: false
  },
  {
    step:4,
    status: "pure",
    isActive: true
  },
  {
    step:5,
    status: "lock",
    isActive: false
  },
  {
    step:6,
    status: "lock",
    isActive: false
  },
  {
    step:7,
    status: "lock",
    isActive: false
  },
]
