import { Component, PropTypes } from 'react';
import TaskNavigationItem from './__item/TaskNavigationItem';

import s from './TaskNavigation.css';
/**
 * Пенель навигации в упражнении
 */
export default class TaskNavigation extends Component {
  onSelect(i) {
    this.props.onSelect(i);
  }
  render() {
    const {items} = this.props;
    return (
      <div className={s.root}>
        {items.map((item,i) => <TaskNavigationItem key={i} {...item} onClick={this.onSelect.bind(this,i)}/>)}
      </div>
    )
  }
}

TaskNavigation.propTypes = {
  /**
   * Элементы навигации
   */
  items: PropTypes.array.isRequired,
  /**
   * item click callback,  @param {Number} index of selected item
   */
  onSelect: PropTypes.func.isRequired
}
