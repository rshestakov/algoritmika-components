var path = require('path');
var glob = require('glob');

module.exports = {
  title: 'Algoritmika Style Guide',
  defaultExample: false,
  components: function() {
  return glob.sync(path.resolve(__dirname, './lib/components/**/*.js')).filter(function(module) {
    return /\/[A-Z]\w*\.js$/.test(module);
  });
},
  updateWebpackConfig: function(webpackConfig, env) {
   let dir = path.join(__dirname, 'lib');
   webpackConfig.module.loaders.push(
     {
       test: /\.css$/,
       include: dir,
       loader: 'style!css?modules!postcss'
     },
     {
         test: /\.(jsx|js)$/,
         include: dir,
         loader: 'babel?cacheDirectory'
     },
     {
       test: /\.(jpe?g|png|ttf|svg|json)$/,
       include: dir,
       loader: 'file'
     }
   );
   return webpackConfig;
  },
};
